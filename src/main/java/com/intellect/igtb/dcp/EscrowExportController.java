package com.intellect.igtb.dcp;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.intellect.igtb.dcp.cloudbocommons.gqlactions.models.ICBXBackendResponse;
import com.intellect.igtb.dcp.export.controller.ExportServiceController;
import com.intellect.igtb.dcp.service.AggregationTwoSummary;
import com.intellect.igtb.dcp.service.AggregationOneSummary;
import com.intellect.igtb.dcp.service.PrincipalSummary;
import com.intellect.igtb.dcp.service.PrincipalExport;
import com.intellect.igtb.dcp.service.PrincipalForHasura;

@RestController
@RequestMapping("/export")
public class EscrowExportController {

	/**
	 * include dependency
	 */

	// demo api
	@Autowired
	PrincipalExport principalExport;

	@Autowired
	PrincipalForHasura principalForHasura;

	// for principal
	@Autowired
	PrincipalSummary principalSummary;

//	for aggregation 1
	@Autowired
	AggregationOneSummary aggregationOneSummary;

//	for aggregation 2
	@Autowired
	AggregationTwoSummary aggregationTwoSummary;

	@RequestMapping(value = "/principalForHasura", method = RequestMethod.POST)
	public ResponseEntity<ICBXBackendResponse> principalForHasura(@RequestBody final Map<String, Object> payload,
			@RequestHeader final Map<String, String> headers) {
		System.out.println("in principal");
		return new ExportServiceController(principalForHasura).execute(payload, headers);
	}

	@RequestMapping(value = "/{casetype}/{card}/{tab}", method = RequestMethod.POST)
	public ResponseEntity<ICBXBackendResponse> apartment(@RequestBody final Map<String, Object> payload,
			@RequestHeader final Map<String, String> headers, @PathVariable String casetype, @PathVariable String card,
			@PathVariable String tab) {

		if (casetype.contains("landlord-tenant") || casetype.contains("barriester")) {
			if (card.contains("master")) {
				if (tab.contains("principal")) {
					return new ExportServiceController(principalSummary).execute(payload, headers);
				} else if (tab.contains("agg1")) {
					return new ExportServiceController(aggregationOneSummary).execute(payload, headers);
				} else if (tab.contains("agg2")) {
					return new ExportServiceController(aggregationTwoSummary).execute(payload, headers);
				} else if (tab.contains("counterparty")) {

				}
			} else if (card.contains("allocations")) {

			} else if (card.contains("fundtransfer")) {

			} else if (card.contains("earnings")) {

			} else if (card.contains("closeaccounts")) {

			}
		}

		return null;

	}

}