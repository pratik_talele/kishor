package com.intellect.igtb.dcp.utilities;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.intellect.igtb.dcp.caf.gqlactions.models.Context;
import com.intellect.igtb.dcp.cloudbocommons.connector.models.ResponsePayLoad;
import com.intellect.igtb.dcp.cloudbocommons.exception.CBXBusinessException;
import com.intellect.igtb.dcp.cloudbocommons.exception.CBXException;
import com.intellect.igtb.dcp.cloudbocommons.gqlactions.models.FieldValidationResult;
import com.intellect.igtb.dcp.cloudbocommons.gqlactions.models.FieldValidationResult.val_status;
import com.intellect.igtb.dcp.cloudbocommons.gqlactions.models.GQLResponse;
import com.intellect.igtb.dcp.cloudbocommons.utils.text.MutationUtils;
import com.intellect.igtb.dcp.cloudbocommons.utils.validation.JsonPathUtils;
import com.intellect.igtb.dcp.export.model.ExportContext;

import net.minidev.json.JSONArray;

public class Utils {
	
	private static final Logger logger = LoggerFactory.getLogger(Utils.class);

	/** The Constant VA_SUMMARY_LIST. */
	private static final String VA_SUMMARY_LIST = "/`hasuraormconfig/principal.gql";


	public Map<String, Object> getVaTransactionSummary(Map<String, Object> payload, final Context context)
			throws CBXException, CBXBusinessException {
		logger.debug("IN - getVaTransactionSummary");

		final HashMap<String, Object> inpMap = new HashMap<String, Object>();

//		String accountId = (String) payload.get("accountId");
//		String currency = (String) payload.get("currency");
//		inpMap.put("accountId", accountId);
//		inpMap.put("currency", currency);
		// argumentMap.put("status", Collections.singletonMap("_neq", "CLOSED"));
//		HashMap<String, Object> searchOptionMap = new HashMap<>();
//		HashMap<String, Object> transactionFilterOption = new HashMap<>();

//		inpMap.put("searchOption", searchOptionMap);
//		inpMap.put("transactionFilterOption", transactionFilterOption);

		Map<String, Object> va_transaction_summary = new HashMap<>();

		final String responsePayload = getQueryResponse(context, VA_SUMMARY_LIST, inpMap);

		logger.info("Reponse Payload{}", responsePayload);

		JSONArray jsonArray = JsonPathUtils.findArrayElementsAtPath(responsePayload,
				"$.data.escrow_va_account_master_work");
		logger.info("getVaTransactionSummary "
				+ JsonPathUtils.findArrayElementsAtPath(responsePayload, "$.data.escrow_va_account_master_work"));

		va_transaction_summary.put("VA_SUMMARY_LIST", jsonArray);
		logger.debug("OUT - VA_SUMMARY_LIST {}", va_transaction_summary.toString());
		return va_transaction_summary;
	}

	public String convertModelIntoJson(final Object request) {
		/** mapper */
		final ObjectMapper mapper = new ObjectMapper();
		final Map<String, Object> map = mapper.convertValue(request, Map.class);
		final StringBuffer jsonBuffer = new StringBuffer();
		MutationUtils.convertHashMapToMutationJSON(map, jsonBuffer);
		return jsonBuffer.toString();
	}
	
	public String getQueryResponse(final Context context, final String lookupTemplate,
			final HashMap<String, Object> queryVariables) {
		String lookUpQry = context.getHasuraConfig().getMutationTemplate(lookupTemplate);
		GQLResponse gqlResponse = null;
		try {
			// Fire the query.
			ResponsePayLoad responsePayload = context.getHttpConnector().executeGQLRequest(lookUpQry, queryVariables,
					context.getHeaders());
			if (!responsePayload.isValid())
				throw new CBXException(responsePayload.getErrorCode(), responsePayload.getErrorMessage());
			// Parse the query output , checking in product
			gqlResponse = new GQLResponse(responsePayload.getPayload());
		} catch (CBXException ex) {
			logger.debug("Exception {}", ex.getStackTrace().toString());
			ex.printStackTrace();
		}
		return gqlResponse.getResponsePayload();
	}
	
	public void addValidationResult(final FieldValidationResult result,
			final Map<String, FieldValidationResult> valResponseMap) {
		if (result.getStatus() == val_status.FAIL) {
			valResponseMap.put(result.getErrorCode(), result);
		}
	}

	public Map<String, Object> getVaTrnsactionSummary(Map<String, Object> payload, ExportContext exportContext) {
		// TODO Auto-generated method stub
		return null;
	}


}
