package com.intellect.igtb.dcp.utilities;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.intellect.igtb.dcp.caf.gqlactions.models.Context;
import com.intellect.igtb.dcp.cloudbocommons.connector.models.ResponsePayLoad;
import com.intellect.igtb.dcp.cloudbocommons.exception.CBXBusinessException;
import com.intellect.igtb.dcp.cloudbocommons.exception.CBXException;
import com.intellect.igtb.dcp.cloudbocommons.gqlactions.models.FieldValidationResult;
import com.intellect.igtb.dcp.cloudbocommons.gqlactions.models.GQLResponse;
import com.intellect.igtb.dcp.cloudbocommons.gqlactions.models.FieldValidationResult.val_status;
import com.intellect.igtb.dcp.cloudbocommons.utils.text.MutationUtils;
import com.intellect.igtb.dcp.cloudbocommons.utils.validation.JsonPathUtils;
import com.intellect.igtb.dcp.export.model.ExportContext;
import com.intellect.igtb.dcp.models.IEscrowExportFields.gqlActionEnum;
import com.nimbusds.jose.shaded.json.JSONObject;

import net.minidev.json.JSONArray;

@SuppressWarnings("unchecked")
@Service
public class Utilities {

	// the logger
	private static final Logger logger = LoggerFactory.getLogger(Utilities.class);

	// hasura queries

	// query for demo
	private static final String VA_SUMMARY_LIST = "/hasuraormconfig/principal.gql";
	private static final String VA_PRINCIPAL_LIST = "/hasuraormconfig/principalPractice.gql";

	// Master tab query
	private static final String VA_TRANSCATION_SUMMARY = "/hasuraormconfig/MasterAccountsSummary.gql";
	private static final String VA_MASTER_TRANSCATION_SUMMARY = "/hasuraormconfig/MasterAccountsTransSummary.gql";
	private static final String VA_MASTER_SEARCH_SUMMARY = "/hasuraormconfig/masterCardSearch.gql";

	// building tab query
	private static final String VA_BUILDING_ACCOUNT_SUMMARY = "/hasuraormconfig/buildingAccountsSummary.gql";
	private static final String VA_BUILDING_ACCOUNT_GROUPBY_SUMMARY = "/hasuraormconfig/buildingAccountsGroupByMasterSummary.gql";
	private static final String VA_BUILDING_ACCOUNT_SEARCH = "/hasuraormconfig/buildingAccountBySearch.gql";

	// apartment tab query
	private static final String VA_APPARTMENT_SUMMARY = "/hasuraormconfig/apartmentAccountSummary.gql";
	private static final String VA_APPARTMENT_GROUPBY_MASTER_SUMMARY = "/hasuraormconfig/apartmentSummaryGroupByMaster.gql";

	public Map<String, Object> getVASummaryList(Map<String, Object> payload, final Context context)
			throws CBXException, CBXBusinessException {
		logger.debug("IN - getPrincipalData");
		final HashMap<String, Object> inpMap = new HashMap<String, Object>();
		HashMap<String, Object> argumentMap = new HashMap<>();

//		String groupCriteria = (String) payload.get("groupCriteria");
//		if (StringUtils.isNotEmpty(groupCriteria) && StringUtils.isNotBlank(groupCriteria)) {
//			if (groupCriteria.equals("principal")) {
//				argumentMap.put("gbprincipal", true);
//			}
//		} else {
//			argumentMap.put("gbphys_acc", false);
//		}
//		

//		Object accountOwnerType =payload.get("searchText");
//		inpMap.put("accountOwnerType", accountOwnerType);

//		HashMap<String, Object> searchOptionMap = new HashMap<>();
//		String searchText = (String) payload.get("searchText");
//		if (StringUtils.isNotEmpty(searchText) && StringUtils.isNotBlank(searchText)) {
//			searchOptionMap.put("accountOwnerType", Collections.singletonMap("_eq", searchText));
//			
//		}

//		change in inpmap for checking commans
//		inpMap.put("argument", argumentMap);
//		inpMap.put("searchOption", searchOptionMap);
		Map<String, Object> va_summary = new HashMap<>();

		final String responsePayload = getQueryResponse(context, VA_SUMMARY_LIST, inpMap);

		logger.info("Reponse Payload{}", responsePayload);

		JSONArray jsonArray = JsonPathUtils.findArrayElementsAtPath(responsePayload,
				"$.data.escrow_va_account_master_work");
		logger.info("get all principal accounts"
				+ JsonPathUtils.findArrayElementsAtPath(responsePayload, "$.data.escrow_va_account_master_work"));

//		JSONArray jsonArray = JsonPathUtils.findArrayElementsAtPath(responsePayload,
//				"$.data.escrow_va_account_master");
//		logger.info("get all principal accounts"
//				+ JsonPathUtils.findArrayElementsAtPath(responsePayload, "$.data.escrow_va_account_master"));

		va_summary.put("vam_summary", jsonArray);
		logger.debug("OUT - getVASummaryList {}", va_summary.toString());
		return va_summary;
	}

	public String convertModelIntoJson(final Object request) {

		final ObjectMapper mapper = new ObjectMapper();
		final Map<String, Object> map = mapper.convertValue(request, Map.class);
		final StringBuffer jsonBuffer = new StringBuffer();
		MutationUtils.convertHashMapToMutationJSON(map, jsonBuffer);
		return jsonBuffer.toString();
	}

	public String getQueryResponse(final Context context, final String lookupTemplate,
			final HashMap<String, Object> queryVariables) {
		String lookUpQry = context.getHasuraConfig().getMutationTemplate(lookupTemplate);
		System.out.println(lookUpQry);

		GQLResponse gqlResponse = null;
		try {
			// Fire the query.
			ResponsePayLoad responsePayload = context.getHttpConnector().executeGQLRequest(lookUpQry, queryVariables,
					context.getHeaders());

			if (!responsePayload.isValid())
				throw new CBXException(responsePayload.getErrorCode(), responsePayload.getErrorMessage());
			// Parse the query output , checking in product
			gqlResponse = new GQLResponse(responsePayload.getPayload());
		} catch (CBXException ex) {
			logger.debug("Exception {}", ex.getStackTrace().toString());
			ex.printStackTrace();
		}
		return gqlResponse.getResponsePayload();
	}

	public void addValidationResult(final FieldValidationResult result,
			final Map<String, FieldValidationResult> valResponseMap) {
		if (result.getStatus() == val_status.FAIL) {
			valResponseMap.put(result.getErrorCode(), result);
		}
	}

	public Map<String, Object> getVAmasterAccountsSummary(Map<String, Object> payload, final Context context)
			throws CBXException, CBXBusinessException {
		logger.debug("IN - getVAmasterAccountsSummary");

		final HashMap<String, Object> inpMap = new HashMap<String, Object>();
		HashMap<String, Object> argumentMap = new HashMap<>();

//		String groupCriteria = (String) payload.get("groupCriteria");
//		if (StringUtils.isNotEmpty(groupCriteria) && StringUtils.isNotBlank(groupCriteria)) {
//			if (groupCriteria.equals("principal")) {
//				argumentMap.put("gbprincipal", true);
//			}
//		} else {
//			argumentMap.put("gbphys_acc", true);
//		}

//		Object accountOwnerType =payload.get("searchText");
//		inpMap.put("accountOwnerType", accountOwnerType);

//		HashMap<String, Object> searchOptionMap = new HashMap<>();
//		String searchText = (String) payload.get("searchText");
//		if (StringUtils.isNotEmpty(searchText) && StringUtils.isNotBlank(searchText)) {
//			searchOptionMap.put("accountOwnerType", Collections.singletonMap("_eq", searchText));
//			
//		}

//		change in inpmap for checking commans
//		inpMap.put("argument", argumentMap);
//		inpMap.put("searchOption", searchOptionMap);
		
		Map<String, Object> va_summary = new HashMap<>();

		final String responsePayload = getQueryResponse(context, VA_TRANSCATION_SUMMARY, inpMap);

		logger.info("Reponse Payload{}", responsePayload);

		JSONArray jsonArray = JsonPathUtils.findArrayElementsAtPath(responsePayload, "$.data.common_accounts");
		logger.info("get all master accounts"
				+ JsonPathUtils.findArrayElementsAtPath(responsePayload, "$.data.common_accounts"));

		va_summary.put("masterAccountsSummary", jsonArray);
		logger.debug("OUT - getVASummaryList {}", va_summary.toString());
		return va_summary;
	}

	public Map<String, Object> getBuildingAccountSummary(Map<String, Object> payload, final Context context)
			throws CBXException, CBXBusinessException {
		logger.debug("IN - getBuildingAccountSummary");

		final HashMap<String, Object> inpMap = new HashMap<String, Object>();
//		HashMap<String, Object> argumentMap = new HashMap<>();		

		

		JSONObject jsonObject = new JSONObject(payload);

		inpMap.put("searchOptions", ((HashMap<String, Object>) jsonObject.get("searchText")).get("searchOptions"));
//		inpMap.put("limit", ((HashMap<String, Object>) jsonObject.get("searchText")).get("limit"));
//		inpMap.put("offset", ((HashMap<String, Object>) jsonObject.get("searchText")).get("offset"));
		inpMap.put("sortOptions", ((HashMap<String, Object>) jsonObject.get("searchText")).get("sortOptions"));

//		change start
//		JSONObject jsonObject = new JSONObject(payload);
//
//		Map<String, Object> extract = new HashMap<String, Object>();
//		extract.put("payload",
//				((HashMap<String, Object>) ((HashMap<String, Object>) ((HashMap<String, Object>) jsonObject
//						.get("input")).get("summaryPrincipalAccountInput")).get("searchText")));

//		System.out.println(extract);
//		System.out.println(((HashMap<String, Object>) extract.get("payload")).get("groupPageSize"));

//		if(((HashMap<String, Object>) extract.get("payload")).get("searchOptions")!=null) {
//			inpMap.put("searchOptions", ((HashMap<String, Object>) extract.get("payload")).get("searchOptions"));
//		}
	

//		inpMap.put("limit", ((HashMap<String, Object>) extract.get("payload")).get("limit"));
//
//		inpMap.put("offset", ((HashMap<String, Object>) extract.get("payload")).get("offset"));
		
//		if(((HashMap<String, Object>) extract.get("payload")).get("sortOptions")!=null) {
//			inpMap.put("sortOptions", ((HashMap<String, Object>) extract.get("payload")).get("sortOptions"));
//		}
	

//		change end
		
		Map<String, Object> va_summary = new HashMap<>();

		final String responsePayload = getQueryResponse(context, VA_BUILDING_ACCOUNT_SUMMARY, inpMap);

		logger.info("Reponse Payload{}", responsePayload);

		JSONArray jsonArray = JsonPathUtils.findArrayElementsAtPath(responsePayload, "$.data.escrow_va_account_master");
		logger.info("get all BuildingAccountSummary"
				+ JsonPathUtils.findArrayElementsAtPath(responsePayload, "$.data.escrow_va_account_master"));

		va_summary.put("buildingAccountSummary", jsonArray);
		logger.debug("OUT - getBuildingAccountSummary {}", va_summary.toString());
		return va_summary;
	}

	public Map<String, Object> getBuildingAccountsGroupByMaster(Map<String, Object> payload, final Context context)
			throws CBXException, CBXBusinessException {
		logger.debug("IN - getBuildingAccountsGroupByMaster");

		final HashMap<String, Object> inpMap = new HashMap<String, Object>();
//		HashMap<String, Object> argumentMap = new HashMap<>();		


		JSONObject jsonObject = new JSONObject(payload);
//		System.out.println(jsonObject);
//
//		inpMap.put("groupLimit", ((HashMap<String, Object>) jsonObject.get("searchText")).get("groupLimit"));
//		inpMap.put("groupOffset", ((HashMap<String, Object>) jsonObject.get("searchText")).get("groupOffset"));
//		inpMap.put("limit", ((HashMap<String, Object>) jsonObject.get("searchText")).get("limit"));
//		inpMap.put("offset", ((HashMap<String, Object>) jsonObject.get("searchText")).get("offset"));
		inpMap.put("args", ((HashMap<String, Object>) jsonObject.get("searchText")).get("args"));
		inpMap.put("groupSearchOption",
				((HashMap<String, Object>) jsonObject.get("searchText")).get("groupSearchOption"));
		inpMap.put("searchOption", ((HashMap<String, Object>) jsonObject.get("searchText")).get("searchOption"));
		inpMap.put("sortOption", ((HashMap<String, Object>) jsonObject.get("searchText")).get("sortOption"));

//		change start
//		JSONObject jsonObject = new JSONObject(payload);
//
//		Map<String, Object> extract = new HashMap<String, Object>();
//		extract.put("payload",
//				((HashMap<String, Object>) ((HashMap<String, Object>) ((HashMap<String, Object>) jsonObject
//						.get("input")).get("summaryPrincipalAccountInput")).get("searchText")));

//		System.out.println(extract);
//		System.out.println(((HashMap<String, Object>) extract.get("payload")).get("groupPageSize"));

//		inpMap.put("groupLimit", ((HashMap<String, Object>) extract.get("payload")).get("groupLimit"));
//
//		inpMap.put("groupOffset", ((HashMap<String, Object>) extract.get("payload")).get("groupOffset"));
//
//		inpMap.put("limit", ((HashMap<String, Object>) extract.get("payload")).get("limit"));
//
//		inpMap.put("offset", ((HashMap<String, Object>) extract.get("payload")).get("offset"));
		
//		if(((HashMap<String, Object>) extract.get("payload")).get("args")!=null) {
//			inpMap.put("args", ((HashMap<String, Object>) extract.get("payload")).get("args"));
//		}
//	
//		if(((HashMap<String, Object>) extract.get("payload")).get("groupSearchOption")!=null) {
//			inpMap.put("groupSearchOption", ((HashMap<String, Object>) extract.get("payload")).get("groupSearchOption"));
//		}
//	
//		if(((HashMap<String, Object>) extract.get("payload")).get("searchOption")!=null) {
//			inpMap.put("searchOption", ((HashMap<String, Object>) extract.get("payload")).get("searchOption"));
//		}
//		
//		if(((HashMap<String, Object>) extract.get("payload")).get("sortOption")!=null) {
//			inpMap.put("sortOption", ((HashMap<String, Object>) extract.get("payload")).get("sortOption"));
//		}
		

//		change end

		

		Map<String, Object> va_summary = new HashMap<>();

		final String responsePayload = getQueryResponse(context, VA_BUILDING_ACCOUNT_GROUPBY_SUMMARY, inpMap);

		logger.info("Reponse Payload{}", responsePayload);

		JSONArray jsonArray = JsonPathUtils.findArrayElementsAtPath(responsePayload,
				"$.data.escrow_fn_acc_active_summary");
		logger.info("get all building Accounts Group By Master"
				+ JsonPathUtils.findArrayElementsAtPath(responsePayload, "$.data.escrow_fn_acc_active_summary"));

		va_summary.put("buildingAccountsGroupByMaster", jsonArray);
		logger.debug("OUT - getBuildingAccountsGroupByMaster {}", va_summary.toString());
		return va_summary;
	}
	

	public Map<String, Object> getApartmentAccount(Map<String, Object> payload, final Context context)
			throws CBXException, CBXBusinessException {
		logger.debug("IN - getApartmentAccount");

		final HashMap<String, Object> inpMap = new HashMap<String, Object>();

		
		
		JSONObject jsonObject=new JSONObject(payload);
		
		
		inpMap.put("searchOptions", ((HashMap<String, Object>) jsonObject.get("searchText")).get("searchOptions"));
		
		inpMap.put("sortOptions", ((HashMap<String, Object>) jsonObject.get("searchText")).get("sortOptions"));
		

//		JSONObject jsonObject = new JSONObject(payload);
//		inpMap.put("searchOptions", ((HashMap<String, Object>) jsonObject.get("searchText")).get("searchOptions"));
////
////		inpMap.put("offset", ((HashMap<String, Object>) jsonObject.get("searchText")).get("offset"));
////
//		inpMap.put("sortOptions", ((HashMap<String, Object>) jsonObject.get("searchText")).get("sortOptions"));

//		change start
//		JSONObject jsonObject = new JSONObject(payload);
//
//		Map<String, Object> extract = new HashMap<String, Object>();
//		extract.put("payload",
//				((HashMap<String, Object>) ((HashMap<String, Object>) ((HashMap<String, Object>) jsonObject
//						.get("input")).get("summaryPrincipalAccountInput")).get("searchText")));

//		System.out.println(extract);
//		System.out.println(((HashMap<String, Object>) extract.get("payload")).get("groupPageSize"));
		
//		if(((HashMap<String, Object>) extract.get("payload")).get("searchOptions")!=null) {
//			inpMap.put("searchOptions", ((HashMap<String, Object>) extract.get("payload")).get("searchOptions"));
//		}
//		
//
////		inpMap.put("offset", ((HashMap<String, Object>) extract.get("payload")).get("offset"));
//		if(((HashMap<String, Object>) extract.get("payload")).get("sortOptions")!=null) {
//			inpMap.put("sortOptions", ((HashMap<String, Object>) extract.get("payload")).get("sortOptions"));
//		}
	


//		change end
		Map<String, Object> apartment_summary = new HashMap<>();

		final String responsePayload = getQueryResponse(context, VA_APPARTMENT_SUMMARY, inpMap);

		logger.info("Reponse Payload{}", responsePayload);

		JSONArray jsonArray = JsonPathUtils.findArrayElementsAtPath(responsePayload, "$.data.escrow_va_account_master");
		logger.info("get all Apartment Account Summary "
				+ JsonPathUtils.findArrayElementsAtPath(responsePayload, "$.data.escrow_va_account_master"));

		apartment_summary.put("getApartmentAccount", jsonArray);
		logger.debug("OUT - getVASummaryList {}", apartment_summary.toString());
		return apartment_summary;
	}

	
	public Map<String, Object> getApartmentAccountSummary(Map<String, Object> payload, final Context context)
			throws CBXException, CBXBusinessException {
		logger.debug("IN - getApartmentAccountSummary");

		final HashMap<String, Object> inpMap = new HashMap<String, Object>();


		JSONObject jsonObject = new JSONObject(payload);
		inpMap.put("searchOptions", ((HashMap<String, Object>) jsonObject.get("searchText")).get("searchOptions"));

		inpMap.put("offset", ((HashMap<String, Object>) jsonObject.get("searchText")).get("offset"));

		inpMap.put("sortOptions", ((HashMap<String, Object>) jsonObject.get("searchText")).get("sortOptions"));

		Map<String, Object> apartment_summary = new HashMap<>();

		final String responsePayload = getQueryResponse(context, VA_APPARTMENT_SUMMARY, inpMap);

		logger.info("Reponse Payload{}", responsePayload);

		JSONArray jsonArray = JsonPathUtils.findArrayElementsAtPath(responsePayload, "$.data.escrow_va_account_master");
		logger.info("get all Apartment Account Summary "
				+ JsonPathUtils.findArrayElementsAtPath(responsePayload, "$.data.escrow_va_account_master"));

		apartment_summary.put("getApartmentAccountSummary", jsonArray);
		logger.debug("OUT - getVASummaryList {}", apartment_summary.toString());
		return apartment_summary;
	}

	public Map<String, Object> getApartmentAccountGroupByMasterSummary(Map<String, Object> payload,
			final Context context) throws CBXException, CBXBusinessException {
		logger.debug("IN - getApartmentAccountGroupByMasterSummary");

		final HashMap<String, Object> inpMap = new HashMap<String, Object>();


		JSONObject jsonObject = new JSONObject(payload);
//		inpMap.put("groupLimit", ((HashMap<String, Object>) jsonObject.get("searchText")).get("groupLimit"));
//		inpMap.put("groupOffset", ((HashMap<String, Object>) jsonObject.get("searchText")).get("groupOffset"));
//		inpMap.put("limit", ((HashMap<String, Object>) jsonObject.get("searchText")).get("limit"));
//		inpMap.put("offset", ((HashMap<String, Object>) jsonObject.get("searchText")).get("offset"));
		inpMap.put("args", ((HashMap<String, Object>) jsonObject.get("searchText")).get("args"));
		inpMap.put("groupSearchOption",
				((HashMap<String, Object>) jsonObject.get("searchText")).get("groupSearchOption"));
		inpMap.put("searchOption", ((HashMap<String, Object>) jsonObject.get("searchText")).get("searchOption"));
		inpMap.put("sortOption", ((HashMap<String, Object>) jsonObject.get("searchText")).get("sortOption"));

//		change start
//		JSONObject jsonObject = new JSONObject(payload);
//
//		Map<String, Object> extract = new HashMap<String, Object>();
//		extract.put("payload",
//				((HashMap<String, Object>) ((HashMap<String, Object>) ((HashMap<String, Object>) jsonObject
//						.get("input")).get("summaryPrincipalAccountInput")).get("searchText")));

//		System.out.println(extract);
//		System.out.println(((HashMap<String, Object>) extract.get("payload")).get("groupPageSize"));

//		inpMap.put("groupLimit", ((HashMap<String, Object>) extract.get("payload")).get("groupLimit"));
//
//		inpMap.put("groupOffset", ((HashMap<String, Object>) extract.get("payload")).get("groupOffset"));
//
//		inpMap.put("limit", ((HashMap<String, Object>) extract.get("payload")).get("limit"));
//
//		inpMap.put("offset", ((HashMap<String, Object>) extract.get("payload")).get("offset"));
		
//		if(((HashMap<String, Object>) extract.get("payload")).get("args")!=null) {
//			inpMap.put("args", ((HashMap<String, Object>) extract.get("payload")).get("args"));
//		}
//		
//		if(((HashMap<String, Object>) extract.get("payload")).get("groupSearchOption")!=null) {
//			inpMap.put("groupSearchOption", ((HashMap<String, Object>) extract.get("payload")).get("groupSearchOption"));
//		}
//		
//		if(((HashMap<String, Object>) extract.get("payload")).get("searchOption")!=null) {
//			inpMap.put("searchOption", ((HashMap<String, Object>) extract.get("payload")).get("searchOption"));
//		}
//		
//		if(((HashMap<String, Object>) extract.get("payload")).get("sortOption")!=null) {
//			inpMap.put("sortOption", ((HashMap<String, Object>) extract.get("payload")).get("sortOption"));
//		}
		



//		change end
		
		Map<String, Object> apartment_summary = new HashMap<>();

		final String responsePayload = getQueryResponse(context, VA_APPARTMENT_GROUPBY_MASTER_SUMMARY, inpMap);

		logger.info("Reponse Payload{}", responsePayload);

		JSONArray jsonArray = JsonPathUtils.findArrayElementsAtPath(responsePayload,
				"$.data.escrow_fn_acc_active_summary");
		logger.info("get all Apartment Account GroupBy Master Summary"
				+ JsonPathUtils.findArrayElementsAtPath(responsePayload, "$.data.escrow_fn_acc_active_summary"));

		apartment_summary.put("apartmentAccountSummary", jsonArray);
		logger.debug("OUT - getApartmentAccountGroupByMasterSummary {}", apartment_summary.toString());
		return apartment_summary;
	}

	public Map<String, Object> getVAmasterTransSummary(Map<String, Object> payload, final Context context)
			throws CBXException, CBXBusinessException {
		logger.debug("IN - getVAmasterTransSummary");

		final HashMap<String, Object> inpMap = new HashMap<String, Object>();

//		change start
//		JSONObject jsonObject = new JSONObject(payload);
//
//		Map<String, Object> extract = new HashMap<String, Object>();
//		extract.put("payload",
//				((HashMap<String, Object>) ((HashMap<String, Object>) ((HashMap<String, Object>) jsonObject
//						.get("input")).get("summaryPrincipalAccountInput")).get("searchText")));
//
////		System.out.println(extract);
////		System.out.println(((HashMap<String, Object>) extract.get("payload")).get("groupPageSize"));
//
////		inpMap.put("groupPageSize", ((HashMap<String, Object>) extract.get("payload")).get("groupPageSize"));
////
////		inpMap.put("groupOffset", ((HashMap<String, Object>) extract.get("payload")).get("groupOffset"));
////
////		inpMap.put("pageSize", ((HashMap<String, Object>) extract.get("payload")).get("pageSize"));
////
////		inpMap.put("offset", ((HashMap<String, Object>) extract.get("payload")).get("offset"));
//
//		if(((HashMap<String, Object>) extract.get("payload")).get("searchOption")!=null) {
//			inpMap.put("searchOption", ((HashMap<String, Object>) extract.get("payload")).get("searchOption"));
//		}

//		change end

		JSONObject jsonObject = new JSONObject(payload);
//		inpMap.put("groupPageSize", ((HashMap<String, Object>) jsonObject.get("searchText")).get("groupPageSize"));
//
//		inpMap.put("groupOffset", ((HashMap<String, Object>) jsonObject.get("searchText")).get("groupOffset"));
//
//		inpMap.put("pageSize", ((HashMap<String, Object>) jsonObject.get("searchText")).get("pageSize"));
//
//		inpMap.put("offset", ((HashMap<String, Object>) jsonObject.get("searchText")).get("offset"));
//
		inpMap.put("searchOption", ((HashMap<String, Object>) jsonObject.get("searchText")).get("searchOption"));
//
////		inpMap.put("sortOption", ((HashMap<String, Object>) jsonObject.get("searchText")).get("sortOption"));

		Map<String, Object> master_trans_summary = new HashMap<>();

		final String responsePayload = getQueryResponse(context, VA_MASTER_TRANSCATION_SUMMARY, inpMap);

		logger.info("Reponse Payload{}", responsePayload);

		JSONArray jsonArray = JsonPathUtils.findArrayElementsAtPath(responsePayload,
				"$.data.escrow_vw_account_sumtransactions_gb");
		logger.info("get all Master Trans Summary" + JsonPathUtils.findArrayElementsAtPath(responsePayload,
				"$.data.escrow_vw_account_sumtransactions_gb"));

		master_trans_summary.put("masterTransSummary", jsonArray);
		logger.debug("OUT - getVAmasterTransSummary {}", master_trans_summary.toString());
		return master_trans_summary;
	}

	public Map<String, Object> getVAmasterSearchSummary(Map<String, Object> payload, final Context context)
			throws CBXException, CBXBusinessException {
		logger.debug("IN - getVAmasterSearchSummary");
		
		final HashMap<String, Object> inpMap = new HashMap<String, Object>();

//		JSONObject jsonObject = new JSONObject(payload);
//		System.out.println(jsonObject);
//		inpMap.put("pageSize", ((HashMap<String, Object>) jsonObject.get("searchText")).get("pageSize"));
//
//		inpMap.put("offset", ((HashMap<String, Object>) jsonObject.get("searchText")).get("offset"));
//
//		inpMap.put("searchOption", ((HashMap<String, Object>) jsonObject.get("searchText")).get("searchOption"));
//
//		inpMap.put("sortParameters", ((HashMap<String, Object>) jsonObject.get("searchText")).get("sortParameters"));

//		change start
		JSONObject jsonObject = new JSONObject(payload);

		Map<String, Object> extract = new HashMap<String, Object>();
		extract.put("payload",
				((HashMap<String, Object>) ((HashMap<String, Object>) ((HashMap<String, Object>) jsonObject
						.get("input")).get("summaryPrincipalAccountInput")).get("searchText")));

//		System.out.println(extract);
//		System.out.println(((HashMap<String, Object>) extract.get("payload")).get("groupPageSize"));

		inpMap.put("pageSize", ((HashMap<String, Object>) extract.get("payload")).get("pageSize"));

		inpMap.put("offset", ((HashMap<String, Object>) extract.get("payload")).get("offset"));

		inpMap.put("searchOption", ((HashMap<String, Object>) extract.get("payload")).get("searchOption"));

		inpMap.put("sortParameters", ((HashMap<String, Object>) extract.get("payload")).get("sortParameters"));

//		change end

		
		
		Map<String, Object> master_search_summary = new HashMap<>();

		final String responsePayload = getQueryResponse(context, VA_MASTER_SEARCH_SUMMARY, inpMap);

		logger.info("Reponse Payload{}", responsePayload);

		JSONArray jsonArray = JsonPathUtils.findArrayElementsAtPath(responsePayload, "$.data.common_accounts");
		logger.info("get all master Account by search"
				+ JsonPathUtils.findArrayElementsAtPath(responsePayload, "$.data.common_accounts"));

		master_search_summary.put("masterSearchSummary", jsonArray);
		logger.debug("OUT - getVAmasterSearchSummary {}", master_search_summary.toString());
		return master_search_summary;
	}

	public Map<String, Object> getBuildingAccountsBySearch(Map<String, Object> payload, final Context context)
			throws CBXException, CBXBusinessException {
		logger.debug("IN - getBuildingAccountsBySearch");
		System.out.println("IN - getBuildingAccountsBySearch");

		final HashMap<String, Object> inpMap = new HashMap<String, Object>();

//		JSONObject jsonObject = new JSONObject(payload);
//		System.out.println(jsonObject);
//		inpMap.put("limit", ((HashMap<String, Object>) jsonObject.get("searchText")).get("limit"));
//
//		inpMap.put("offset", ((HashMap<String, Object>) jsonObject.get("searchText")).get("offset"));
//
//		inpMap.put("searchOptions", ((HashMap<String, Object>) jsonObject.get("searchText")).get("searchOptions"));
//
//		inpMap.put("sortOptions", ((HashMap<String, Object>) jsonObject.get("searchText")).get("sortOptions"));
		
//		change start
		JSONObject jsonObject = new JSONObject(payload);

		Map<String, Object> extract = new HashMap<String, Object>();
		extract.put("payload",
				((HashMap<String, Object>) ((HashMap<String, Object>) ((HashMap<String, Object>) jsonObject
						.get("input")).get("summaryPrincipalAccountInput")).get("searchText")));

//		System.out.println(extract);
//		System.out.println(((HashMap<String, Object>) extract.get("payload")).get("groupPageSize"));

		inpMap.put("limit", ((HashMap<String, Object>) extract.get("payload")).get("limit"));

		inpMap.put("offset", ((HashMap<String, Object>) extract.get("payload")).get("offset"));

		inpMap.put("searchOptions", ((HashMap<String, Object>) extract.get("payload")).get("searchOptions"));

		inpMap.put("sortOptions", ((HashMap<String, Object>) extract.get("payload")).get("sortOptions"));

//		change end

		Map<String, Object> apartment_summary = new HashMap<>();

		final String responsePayload = getQueryResponse(context, VA_BUILDING_ACCOUNT_SEARCH, inpMap);

		logger.info("Reponse Payload{}", responsePayload);

		JSONArray jsonArray = JsonPathUtils.findArrayElementsAtPath(responsePayload, "$.data.escrow_va_account_master");
		logger.info("get all building Account by search"
				+ JsonPathUtils.findArrayElementsAtPath(responsePayload, "$.data.escrow_va_account_master"));

		apartment_summary.put("buildingsBySearch", jsonArray);
		logger.debug("OUT - getBuildingAccountsBySearch {}", apartment_summary.toString());
		return apartment_summary;
	}

	
	public Map<String, Object> getVAmasterSearchTransSummary(Map<String, Object> payload, final Context context)
			throws CBXException, CBXBusinessException {
		logger.debug("IN - getVAmasterSearchTransSummary");
		System.out.println("hi from getVAmasterSearchTransSummary");
		
		final HashMap<String, Object> inpMap = new HashMap<String, Object>();
		HashMap<String, Object> argumentMap = new HashMap<>();
		
//		change start
		JSONObject jsonObject = new JSONObject(payload);

		Map<String, Object> extract = new HashMap<String, Object>();
		extract.put("payload",
				((HashMap<String, Object>) ((HashMap<String, Object>) ((HashMap<String, Object>) jsonObject
						.get("input")).get("summaryPrincipalAccountInput")).get("searchText")));

		inpMap.put("groupPageSize", ((HashMap<String, Object>) extract.get("payload")).get("groupPageSize"));

		inpMap.put("groupOffset", ((HashMap<String, Object>) extract.get("payload")).get("groupOffset"));

		inpMap.put("pageSize", ((HashMap<String, Object>) extract.get("payload")).get("pageSize"));

		inpMap.put("offset", ((HashMap<String, Object>) extract.get("payload")).get("offset"));

		inpMap.put("searchOption", ((HashMap<String, Object>) extract.get("payload")).get("searchOption"));

//		change end


		Map<String, Object> master_trans_summary = new HashMap<>();

		final String responsePayload = getQueryResponse(context, VA_MASTER_TRANSCATION_SUMMARY, inpMap);

		logger.info("Reponse Payload{}", responsePayload);

		JSONArray jsonArray = JsonPathUtils.findArrayElementsAtPath(responsePayload,
				"$.data.escrow_vw_account_sumtransactions_gb");
		logger.info("get all Master Trans Summary" + JsonPathUtils.findArrayElementsAtPath(responsePayload,
				"$.data.escrow_vw_account_sumtransactions_gb"));

		master_trans_summary.put("masterTransSummary", jsonArray);
		logger.debug("OUT - getVAmasterTransSummary {}", master_trans_summary.toString());
		return master_trans_summary;

	}

	// change after getting query
	public Map<String, Object> getBuildingAccountsByTrans(Map<String, Object> payload, final Context context)
			throws CBXException, CBXBusinessException {
		logger.debug("IN - getBuildingAccountsByTrans");
		final HashMap<String, Object> inpMap = new HashMap<String, Object>();
		HashMap<String, Object> argumentMap = new HashMap<>();

		Map<String, Object> va_summary = new HashMap<>();

		final String responsePayload = getQueryResponse(context, VA_SUMMARY_LIST, inpMap);

		logger.info("Reponse Payload{}", responsePayload);

		JSONArray jsonArray = JsonPathUtils.findArrayElementsAtPath(responsePayload,
				"$.data.escrow_va_account_master_work");
		logger.info("get all building accounts"
				+ JsonPathUtils.findArrayElementsAtPath(responsePayload, "$.data.escrow_va_account_master_work"));

		va_summary.put("buildingsTrans", jsonArray);
		logger.debug("OUT - getBuildingAccountsByTrans {}", va_summary.toString());
		return va_summary;
	}

	public Map<String, Object> getApartmentAccountBySearch(Map<String, Object> payload, final Context context)
			throws CBXException, CBXBusinessException {
		logger.debug("IN - getBuildingAccountsByTrans");
		final HashMap<String, Object> inpMap = new HashMap<String, Object>();
		HashMap<String, Object> argumentMap = new HashMap<>();

		Map<String, Object> va_summary = new HashMap<>();

		final String responsePayload = getQueryResponse(context, VA_SUMMARY_LIST, inpMap);

		logger.info("Reponse Payload{}", responsePayload);

		JSONArray jsonArray = JsonPathUtils.findArrayElementsAtPath(responsePayload,
				"$.data.escrow_va_account_master_work");
		logger.info("get all apartment Account by search"
				+ JsonPathUtils.findArrayElementsAtPath(responsePayload, "$.data.escrow_va_account_master_work"));

		va_summary.put("apartmentTrans", jsonArray);
		logger.debug("OUT - getBuildingAccountsByTrans {}", va_summary.toString());
		return va_summary;
	}

	public Map<String, Object> getApartmentAccountTrans(Map<String, Object> payload, final Context context)
			throws CBXException, CBXBusinessException {
		logger.debug("IN - getApartmentAccountTrans");
		final HashMap<String, Object> inpMap = new HashMap<String, Object>();
		HashMap<String, Object> argumentMap = new HashMap<>();

		Map<String, Object> va_summary = new HashMap<>();

		final String responsePayload = getQueryResponse(context, VA_SUMMARY_LIST, inpMap);

		logger.info("Reponse Payload{}", responsePayload);

		JSONArray jsonArray = JsonPathUtils.findArrayElementsAtPath(responsePayload,
				"$.data.escrow_va_account_master_work");
		logger.info("get all apartment accounts Transaction"
				+ JsonPathUtils.findArrayElementsAtPath(responsePayload, "$.data.escrow_va_account_master_work"));

		va_summary.put("apartmentTrans", jsonArray);
		logger.debug("OUT - getApartmentAccountTrans {}", va_summary.toString());
		return va_summary;
	}

	public Map<String, Object> getVASummaryForHasuraList(Map<String, Object> payload, final Context context)
			throws CBXException, CBXBusinessException {
		logger.debug("IN - getVASummaryForHasuraList");
		final HashMap<String, Object> inpMap = new HashMap<String, Object>();
		HashMap<String, Object> argumentMap = new HashMap<>();

		JSONObject jsonObject = new JSONObject(payload);

		Map<String, Object> extract = new HashMap<String, Object>();
		extract.put("payload",
				((HashMap<String, Object>) ((HashMap<String, Object>) ((HashMap<String, Object>) jsonObject
						.get("input")).get("summaryPrincipalAccountInput")).get("searchText")));

		System.out.println(extract);


		Map<String, Object> va_summary = new HashMap<>();

		final String responsePayload = getQueryResponse(context, VA_SUMMARY_LIST, inpMap);

		logger.info("Reponse Payload{}", responsePayload);

		JSONArray jsonArray = JsonPathUtils.findArrayElementsAtPath(responsePayload,
				"$.data.escrow_va_account_master_work");
		logger.info("get all getVASummaryForHasuraList accounts"
				+ JsonPathUtils.findArrayElementsAtPath(responsePayload, "$.data.escrow_va_account_master_work"));

		va_summary.put("vam_summary", jsonArray);
		logger.debug("OUT - getVASummaryForHasuraList {}", va_summary.toString());
		return va_summary;
	}

	public Object getValueByField(Map<String, Object> payload, String actionName, String fieldName) {
		Object value = ((Map) ((Map) payload.get("input")).get(actionName)).get(fieldName);
//		System.out.println(" value : " + value);
		return value;
	}

//	public Object getValueByField(Map<String, Object> payload, gqlActionEnum actionName,
//			String fieldName) {
//		Object value =  ((Map) ((Map) payload.get("input")).get(actionName)).get(fieldName);
//		System.out.println(" value : "+value);
//		return value;
//	}
	
	
	public Map<String, Object> getAllApartment(Map<String, Object> payload, final Context context)
			throws CBXException, CBXBusinessException {
		logger.debug("IN - getPrincipalData");
		final HashMap<String, Object> inpMap = new HashMap<String, Object>();
		HashMap<String, Object> argumentMap = new HashMap<>();

//		String groupCriteria = (String) payload.get("groupCriteria");
//		if (StringUtils.isNotEmpty(groupCriteria) && StringUtils.isNotBlank(groupCriteria)) {
//			if (groupCriteria.equals("principal")) {
//				argumentMap.put("gbprincipal", true);
//			}
//		} else {
//			argumentMap.put("gbphys_acc", false);
//		}
//		

//		Object accountOwnerType =payload.get("searchText");
//		inpMap.put("accountOwnerType", accountOwnerType);

//		HashMap<String, Object> searchOptionMap = new HashMap<>();
//		String searchText = (String) payload.get("searchText");
//		if (StringUtils.isNotEmpty(searchText) && StringUtils.isNotBlank(searchText)) {
//			searchOptionMap.put("accountOwnerType", Collections.singletonMap("_eq", searchText));
//			
//		}

//		change in inpmap for checking commans
//		inpMap.put("argument", argumentMap);
//		inpMap.put("searchOption", searchOptionMap);
		
		JSONObject jsonObject=new JSONObject(payload);
		
		inpMap.put("searchOptions", ((HashMap<String, Object>) jsonObject.get("searchText")).get("searchOptions"));
		inpMap.put("sortOptions", ((HashMap<String, Object>) jsonObject.get("searchText")).get("sortOptions"));
		
		Map<String, Object> va_summary = new HashMap<>();

		final String responsePayload = getQueryResponse(context, VA_APPARTMENT_SUMMARY, inpMap);

		logger.info("Reponse Payload{}", responsePayload);

		JSONArray jsonArray = JsonPathUtils.findArrayElementsAtPath(responsePayload,
				"$.data.escrow_va_account_master");
		logger.info("get all principal accounts"
				+ JsonPathUtils.findArrayElementsAtPath(responsePayload, "$.data.escrow_va_account_master"));

//		JSONArray jsonArray = JsonPathUtils.findArrayElementsAtPath(responsePayload,
//				"$.data.escrow_va_account_master");
//		logger.info("get all principal accounts"
//				+ JsonPathUtils.findArrayElementsAtPath(responsePayload, "$.data.escrow_va_account_master"));

		va_summary.put("vam_summary", jsonArray);
		logger.debug("OUT - getVASummaryList {}", va_summary.toString());
		return va_summary;
	}

	
	
	

}


