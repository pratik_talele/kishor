package com.intellect.igtb.dcp.models;

public interface IEscrowExportFields {
	
	enum gqlActionEnum {
		summaryPrincipalAccountInput
		
	}

	String namespace = "escrow";
	public  static final String FORMAT ="format"; 
	public  static final String IS_GROUPED ="isGrouped";   
	public  static final String groupCriteria ="groupCriteria"; 
	public  static final String summary_name ="summary_name"; 
	public  static final String IS_TRANSACTION ="isTransaction"; 
	public  static final String IS_SEARCHED ="isSearched";
}
