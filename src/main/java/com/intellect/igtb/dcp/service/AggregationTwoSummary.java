package com.intellect.igtb.dcp.service;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.intellect.igtb.dcp.caf.gqlactions.models.ActionEnums.system_reject_errorcodes;
import com.intellect.igtb.dcp.cloudbocommons.exception.CBXBusinessException;
import com.intellect.igtb.dcp.cloudbocommons.exception.CBXException;
import com.intellect.igtb.dcp.cloudbocommons.gqlactions.models.FieldValidationResult;
import com.intellect.igtb.dcp.cloudbocommons.gqlactions.models.FieldValidationResult.val_status;
import com.intellect.igtb.dcp.export.model.ExportContext;
import com.intellect.igtb.dcp.export.model.Title;
import com.intellect.igtb.dcp.export.service.CBXSExportService;
import com.intellect.igtb.dcp.export.service.ReportService;
import com.intellect.igtb.dcp.models.IEscrowExportFields;
import com.intellect.igtb.dcp.utilities.Utilities;
import com.nimbusds.jose.shaded.json.JSONObject;

@Component
public class AggregationTwoSummary extends CBXSExportService {

	private static final Logger logger = LoggerFactory.getLogger(AggregationTwoSummary.class);

	private Utilities utils = new Utilities();

	// change
	private String actionName;

	private boolean gbparent;
	private boolean gbmaster;

	HashMap<String, FieldValidationResult> valResponseMap = new HashMap<String, FieldValidationResult>();

	@Autowired
	ReportService reportService;

	@Override
	public ExportContext init(final Map<String, String> httpHeaders, final Map<String, Object> payload)
			throws CBXException {
		logger.debug("IN init - AggregationTwoSummary {}", payload.toString());
		exportContext = super.init(httpHeaders, payload);

		logger.debug("OUT init - AggregationTwoSummary");
		return exportContext;

//		logger.debug("IN init - ApartmentAccountSummary {}", payload.toString());
//		actionName = IEscrowExportFields.gqlActionEnum.summaryPrincipalAccountInput.name();
//		logger.info("actionName : {} ", actionName);
//		exportContext = super.init(httpHeaders, payload);
//		exportContext.setApi_action(actionName);
//		logger.debug("OUT init - ApartmentAccountSummary");
//		return exportContext;

	}

	@Override
	public Map<String, FieldValidationResult> validate() {
		logger.debug("IN -> AggregationTwoSummary.validate");
		final FieldValidationResult valResult = new FieldValidationResult(val_status.OK);
		valResponseMap = new HashMap<String, FieldValidationResult>();
		Map<String, Object> payload = exportContext.getReq_payload();
		String format = (String) payload.get("format");

//		String format = this.getFormat(payload);

		if (format == null || format.isEmpty()) {
			valResult.setFieldName("format");
			valResult.setErrorCode("format required");
			valResult.setStatus(val_status.FAIL);
			String fieldMessage = "format Required";
			valResult.setErrorMessage(fieldMessage);
		}

		utils.addValidationResult(valResult, valResponseMap);
		logger.debug("OUT -> AggregationTwoSummary.validate");
		return valResponseMap;
	}

	@Override
	public String getQueryName(Map<String, Object> payload) {
		String groupCriteria = (String) payload.get("groupCriteria");
		JSONObject jsonObject = new JSONObject(payload);
		String querName = (String) jsonObject.get("summary_name");

		return querName;

//		final String groupCriteria = (String) utils.getValueByField(payload, actionName,
//				IEscrowExportFields.groupCriteria);
//		return (String) utils.getValueByField(payload, actionName, IEscrowExportFields.summary_name);

	}

	@Override
	public String getFormat(Map<String, Object> payload) {
		String format = (String) payload.get("format");
//		final String format = (String) utils.getValueByField(payload, actionName, IEscrowExportFields.FORMAT);

		return format;
	}

	@Override
	public Boolean isGrouped(Map<String, Object> payload) {
//		JSONObject jsonObject=new JSONObject(payload);
//		Boolean isGrouped = (Boolean) payload.get("isGrouped");
//		Boolean isGrouped =(Boolean) jsonObject.get("isGrouped"); 
//		final Boolean isGrouped = (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_GROUPED);

		return false;
	}

	@Override
	public Title getTitle(Map<String, Object> payload) {
		Title title = new Title();
		title.setKey((String) payload.get("summary_name"));
		return title;

//		Title title = new Title();
////		title.setKey((String) payload.get("summary_name"));
//		final String titleKey = (String) utils.getValueByField(payload, actionName, IEscrowExportFields.summary_name);
//
//		title.setKey(titleKey);
//
//		return title;

	}

	@Override
	public String getHeaderBlocksConfig(Map<String, Object> payload) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getGridConfigAsString(Map<String, Object> payload) {
		String sGridConfigData;
		if ((boolean) payload.get("isGrouped") == false && (boolean) payload.get("isSearched") == false
				&& (boolean) payload.get("isTransaction") == false) {
			sGridConfigData = reportService.loadContent("/grid_list/apartmentAccountSummaryGrid.json");
		} else if ((boolean) payload.get("isGrouped") == true) {
			JSONObject jsonObject = new JSONObject(payload);

//			Map<String, Object> extract = new HashMap<String, Object>();
//			extract.put("payload",
//					((HashMap<String, Object>) ((HashMap<String, Object>) ((HashMap<String, Object>) jsonObject
//							.get("input")).get("summaryPrincipalAccountInput")).get("searchText")));

//			System.out.println(extract);
			try {

//				gbparent = (boolean) ((HashMap<String, Object>) ((HashMap<String, Object>) extract.get("payload"))
//						.get("args")).get("gbparent");
				gbparent = (boolean) ((HashMap<String, Object>) ((HashMap<String, Object>) payload.get("searchText"))
						.get("args")).get("gbparent");

			} catch (Exception e) {
				System.out.println(e);
			}

			try {
//				gbmaster = (boolean) ((HashMap<String, Object>) ((HashMap<String, Object>) extract.get("payload"))
//						.get("args")).get("gbmaster");
				gbmaster = (boolean) ((HashMap<String, Object>) ((HashMap<String, Object>) payload.get("searchText"))
						.get("args")).get("gbmaster");

			} catch (Exception e) {
				// TODO: handle exception
			}

			if (gbparent == true) {

				System.out.println("hello from new code");
				sGridConfigData = reportService.loadContent("/grid_list/apartmentGroupByBuilding.json");

			} else

			if (gbmaster == true) {
				System.out.println("hello from new new code");
				sGridConfigData = reportService.loadContent("/grid_list/apartmentAccountGroupByGrid.json");
			} else {
				sGridConfigData = null;
			}

		} else if ((boolean) payload.get("isSearched") == true) {
			sGridConfigData = reportService.loadContent("/grid_list/apartmentAccountGroupByGrid.json");
		} else if ((boolean) payload.get("isTransaction") == true) {
			sGridConfigData = reportService.loadContent("/grid_list/apartmentAccountGroupByGrid.json");
		}

		else {
			sGridConfigData = null;
		}

//		hasura changes start

//		if ((Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_GROUPED) == false
//				&& (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_TRANSACTION) == false
//				&& (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_SEARCHED) == false) {
//			sGridConfigData = reportService.loadContent("/grid_list/apartmentAccountSummaryGrid.json");
//
//		} else if ((Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_GROUPED) == true
//				&& (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_TRANSACTION) == false
//				&& (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_SEARCHED) == false) {
//			// change start
//
//			JSONObject jsonObject = new JSONObject(payload);
//
//			Map<String, Object> extract = new HashMap<String, Object>();
//			extract.put("payload",
//					((HashMap<String, Object>) ((HashMap<String, Object>) ((HashMap<String, Object>) jsonObject
//							.get("input")).get("summaryPrincipalAccountInput")).get("searchText")));
//
////			System.out.println(extract);
//			try {
//
//				gbparent = (boolean) ((HashMap<String, Object>) ((HashMap<String, Object>) extract.get("payload"))
//						.get("args")).get("gbparent");
//
//			} catch (Exception e) {
//				System.out.println(e);
//			}
//
//			try {
//				System.out.println("in gb master");
//				gbmaster = (boolean) ((HashMap<String, Object>) ((HashMap<String, Object>) extract.get("payload"))
//						.get("args")).get("gbmaster");
//
//			} catch (Exception e) {
//				// TODO: handle exception
//			}
//
//			if (gbparent == true) {
//
//				System.out.println("hello from new code");
//				sGridConfigData = reportService.loadContent("/grid_list/apartmentGroupByBuilding.json");
//
//			} else
//
//			if (gbmaster == true) {
//				System.out.println("hello from new new code");
//				sGridConfigData = reportService.loadContent("/grid_list/apartmentAccountGroupByGrid.json");
//			} else {
//				sGridConfigData = null;
//			}
//
//			// change end
//		} else if ((Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_GROUPED) == false
//				&& (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_TRANSACTION) == false
//				&& (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_SEARCHED) == true) {
//			sGridConfigData = reportService.loadContent("/grid_list/apartmentAccountGroupByGrid.json");
//		}
//
//		else if ((Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_GROUPED) == false
//				&& (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_TRANSACTION) == true
//				&& (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_SEARCHED) == false) {
//			sGridConfigData = reportService.loadContent("/grid_list/apartmentAccountGroupByGrid.json");
//		}
//
//		else {
//			System.out.println("hi from else");
//			sGridConfigData = null;
//		}

//		hasura changes end
		return sGridConfigData;
	}

	@Override
	public String getHeaderData(Map<String, Object> payload) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getExportData(Map<String, Object> payload) {
		String sExportData = "";
		try {
			if((boolean) payload.get("isGrouped")==false && (boolean) payload.get("isSearched")==false && (boolean) payload.get("isTransaction")==false) {
				Map<String, Object> masterAccountsSummary = utils.getAllApartment(payload, exportContext);
				sExportData = masterAccountsSummary.get("vam_summary").toString();
			}else if((boolean) payload.get("isGrouped")==true) {
				Map<String, Object> masterAccountsSummary = utils.getApartmentAccountGroupByMasterSummary(payload, exportContext);
				sExportData = masterAccountsSummary.get("apartmentAccountSummary").toString();
			}else if((boolean) payload.get("isSearched")==true) {
				Map<String, Object> masterAccountsSummary = utils.getApartmentAccountBySearch(payload, exportContext);
				sExportData = masterAccountsSummary.get("apartmentTrans").toString();
			}else if ((boolean) payload.get("isTransaction")==true) {
				Map<String, Object> masterAccountsSummary = utils.getApartmentAccountTrans(payload, exportContext);
				sExportData = masterAccountsSummary.get("apartmentTrans").toString();
			}

//			hasura changes start
			
//			if ((Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_GROUPED) == false
//					&& (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_TRANSACTION) == false
//					&& (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_SEARCHED) == false) {
//				Map<String, Object> masterAccountsSummary = utils.getApartmentAccount(payload, exportContext);
//				sExportData = masterAccountsSummary.get("getApartmentAccount").toString();
//			} else if ((Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_GROUPED) == true
//					&& (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_TRANSACTION) == false
//					&& (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_SEARCHED) == false) {
//				Map<String, Object> masterAccountsSummary = utils.getApartmentAccountGroupByMasterSummary(payload,
//						exportContext);
//				sExportData = masterAccountsSummary.get("apartmentAccountSummary").toString();
//			} else if ((Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_GROUPED) == false
//					&& (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_TRANSACTION) == false
//					&& (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_SEARCHED) == true) {
//				Map<String, Object> masterAccountsSummary = utils.getApartmentAccountBySearch(payload, exportContext);
//				sExportData = masterAccountsSummary.get("apartmentTrans").toString();
//			} else if ((Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_GROUPED) == false
//					&& (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_TRANSACTION) == true
//					&& (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_SEARCHED) == false) {
//				Map<String, Object> masterAccountsSummary = utils.getApartmentAccountTrans(payload, exportContext);
//				sExportData = masterAccountsSummary.get("apartmentTrans").toString();
//			}
			
//			hasura changes end

		} catch (CBXBusinessException | CBXException e) {
			logger.debug("Exception {}", e.getStackTrace().toString());
		}
		return sExportData;
	}

}
