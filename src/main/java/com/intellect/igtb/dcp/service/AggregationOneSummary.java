package com.intellect.igtb.dcp.service;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonRawValue;
import com.intellect.igtb.dcp.cloudbocommons.exception.CBXBusinessException;
import com.intellect.igtb.dcp.cloudbocommons.exception.CBXException;
import com.intellect.igtb.dcp.cloudbocommons.gqlactions.models.FieldValidationResult;
import com.intellect.igtb.dcp.cloudbocommons.gqlactions.models.FieldValidationResult.val_status;
import com.intellect.igtb.dcp.export.model.ExportContext;
import com.intellect.igtb.dcp.export.model.Title;
import com.intellect.igtb.dcp.export.service.CBXSExportService;
import com.intellect.igtb.dcp.export.service.ReportService;
import com.intellect.igtb.dcp.models.IEscrowExportFields;
import com.intellect.igtb.dcp.utilities.Utilities;
import com.nimbusds.jose.shaded.json.JSONObject;

@Component
public class AggregationOneSummary extends CBXSExportService {
	
	private static final Logger logger = LoggerFactory.getLogger(AggregationOneSummary.class);


	private Utilities utils=new Utilities();
	
	// change
	private String actionName;
	
	HashMap<String, FieldValidationResult> valResponseMap = new HashMap<String, FieldValidationResult>();

	@Autowired
	ReportService reportService;

	@Override
	public ExportContext init(final Map<String, String> httpHeaders, final Map<String, Object> payload)
			throws CBXException {
//		logger.info("IN init - BuildingAccountSummary {}", payload.toString());
////		httpHeaders.put("x-hasura-admin-secret", "cbxvam");
//		exportContext = super.init(httpHeaders, payload);
////		System.out.println(exportContext.getHttp_headers());
////		System.exit(0);
//		logger.debug("OUT init - BuildingAccountSummary");
//		return exportContext;
		
		logger.debug("IN init - AggregationOneSummary {}", payload.toString());

		exportContext = super.init(httpHeaders, payload);

		logger.debug("OUT init - AggregationOneSummary");
		return exportContext;
		
		
//		logger.debug("IN init - BuildingAccountSummary {}", payload.toString());
//		actionName = IEscrowExportFields.gqlActionEnum.summaryPrincipalAccountInput.name();
//		logger.info("actionName : {} ", actionName);
//		exportContext = super.init(httpHeaders, payload);
//		exportContext.setApi_action(actionName);
//		logger.debug("OUT init - BuildingAccountSummary");
//		return exportContext;
		
	}

	@Override
	public Map<String, FieldValidationResult> validate() {
		logger.debug("IN -> AggregationOneSummary.validate");
		final FieldValidationResult valResult = new FieldValidationResult(val_status.OK);
		valResponseMap = new HashMap<String, FieldValidationResult>();
		Map<String, Object> payload = exportContext.getReq_payload();
		String format = (String) payload.get("format");

//		String format = this.getFormat(payload);
		
		if (format == null || format.isEmpty()) {
			valResult.setFieldName("format");
			valResult.setErrorCode("format required");
			valResult.setStatus(val_status.FAIL);
			String fieldMessage = "format Required";
			valResult.setErrorMessage(fieldMessage);
		}

		utils.addValidationResult(valResult, valResponseMap);
		logger.debug("OUT -> AggregationOneSummary.validate");
		return valResponseMap;
	}

	@Override
	public String getQueryName(Map<String, Object> payload) {
		String groupCriteria = (String) payload.get("groupCriteria");
//		final String groupCriteria = (String) utils.getValueByField(payload, actionName,
//				IEscrowExportFields.groupCriteria);
		JSONObject jsonObject=new JSONObject(payload);
		String querName = (String) jsonObject.get("summary_name");
		return querName;

	}

	@Override
	public String getFormat(Map<String, Object> payload) {
		String format = (String) payload.get("format");
//		final String format = (String) utils.getValueByField(payload, actionName, IEscrowExportFields.FORMAT);
		return format;
	}

	@Override
	public Boolean isGrouped(Map<String, Object> payload) {
//		JSONObject jsonObject=new JSONObject(payload);
//		Boolean isGrouped = (Boolean) payload.get("isGrouped");
//		Boolean isGrouped =(Boolean) jsonObject.get("isGrouped"); 
//		final Boolean isGrouped = (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_GROUPED);
		return false;
	}

	@Override
	public Title getTitle(Map<String, Object> payload) {
		Title title = new Title();
		title.setKey((String) payload.get("summary_name"));
		return title;
		
//		Title title = new Title();
////		title.setKey((String) payload.get("summary_name"));
//		final String titleKey = (String) utils.getValueByField(payload, actionName, IEscrowExportFields.summary_name);
//
//		title.setKey(titleKey);

//		return title;
	}

	@Override
	public String getHeaderBlocksConfig(Map<String, Object> payload) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getGridConfigAsString(Map<String, Object> payload) { 
		String sGridConfigData;
		if((boolean)payload.get("isGrouped")==false && (boolean)payload.get("isSearched")==false) {
			sGridConfigData = reportService.loadContent("/grid_list/buildingAccountSummaryGrid.json");
		}else if((boolean)payload.get("isGrouped")==true) {
			sGridConfigData = reportService.loadContent("/grid_list/buildingAccountGroupByMasterSummaryGrid.json");
		}else if((boolean)payload.get("isSearched")==true) {
			sGridConfigData = reportService.loadContent("/grid_list/buildingAccountSummaryGrid.json");
		}else if((boolean)payload.get("isTransaction")==true) {
			// change after getting query
			sGridConfigData = reportService.loadContent("/grid_list/buildingAccountSummaryGrid.json");
		}	
		else {
			sGridConfigData=null;
		}
//		hasura changes start
//		if ((Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_GROUPED) == false
//				&& (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_TRANSACTION) == false
//				&& (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_SEARCHED) == false) {
//			sGridConfigData = reportService.loadContent("/grid_list/buildingAccountSummaryGrid.json");
//
//		} else if ((Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_GROUPED) == true
//				&& (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_TRANSACTION) == false
//				&& (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_SEARCHED) == false) {
//			sGridConfigData = reportService.loadContent("/grid_list/buildingAccountGroupByMasterSummaryGrid.json");
//		} else if ((Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_GROUPED) == false
//				&& (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_TRANSACTION) == false
//				&& (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_SEARCHED) == true) {
//			sGridConfigData = reportService.loadContent("/grid_list/buildingAccountSummaryGrid.json");
//		}
//
//		else if ((Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_GROUPED) == false
//				&& (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_TRANSACTION) == true
//				&& (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_SEARCHED) == false) {
//			sGridConfigData = reportService.loadContent("/grid_list/buildingAccountSummaryGrid.json");
//		}
//
//		else {
//			System.out.println("hi from else");
//			sGridConfigData = null;
//		}
		
//		hasura changes end

		return sGridConfigData;
	}

	@Override
	public String getHeaderData(Map<String, Object> payload) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getExportData(Map<String, Object> payload) {
		String sExportData = "";
		try {
	
			if((boolean) payload.get("isGrouped")==false && (boolean) payload.get("isSearched")==false && (boolean) payload.get("isTransaction")==false ) {
				Map<String, Object> masterAccountsSummary = utils.getBuildingAccountSummary(payload, exportContext);
				sExportData = masterAccountsSummary.get("buildingAccountSummary").toString();
			}else if((boolean) payload.get("isGrouped")==true){
				Map<String, Object> masterAccountsSummary = utils.getBuildingAccountsGroupByMaster(payload, exportContext);
				sExportData = masterAccountsSummary.get("buildingAccountsGroupByMaster").toString();
			}else if((boolean) payload.get("isSearched")==true) {
				Map<String, Object> masterAccountsSummary = utils.getBuildingAccountsBySearch(payload, exportContext);
				sExportData = masterAccountsSummary.get("buildingsBySearch").toString();
			}else if((boolean) payload.get("isTransaction")==true) {
				Map<String, Object> masterAccountsSummary = utils.getBuildingAccountsByTrans(payload, exportContext);
				sExportData = masterAccountsSummary.get("buildingsTrans").toString();
			}
			
//			hasura changes start
			
//			if ((Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_GROUPED) == false
//					&& (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_TRANSACTION) == false
//					&& (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_SEARCHED) == false) {
//				Map<String, Object> masterAccountsSummary = utils.getBuildingAccountSummary(payload, exportContext);
//				sExportData = masterAccountsSummary.get("buildingAccountSummary").toString();
//			} else if ((Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_GROUPED) == true
//					&& (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_TRANSACTION) == false
//					&& (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_SEARCHED) == false) {
//				Map<String, Object> masterAccountsSummary = utils.getBuildingAccountsGroupByMaster(payload, exportContext);
//				sExportData = masterAccountsSummary.get("buildingAccountsGroupByMaster").toString();
//			} else if ((Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_GROUPED) == false
//					&& (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_TRANSACTION) == false
//					&& (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_SEARCHED) == true) {
//				Map<String, Object> masterAccountsSummary = utils.getBuildingAccountsBySearch(payload, exportContext);
//				sExportData = masterAccountsSummary.get("buildingsBySearch").toString();
//			} else if ((Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_GROUPED) == false
//					&& (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_TRANSACTION) == true
//					&& (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_SEARCHED) == false) {
//				System.out.println("hello");
//				Map<String, Object> masterAccountsSummary = utils.getBuildingAccountsByTrans(payload, exportContext);
//				sExportData = masterAccountsSummary.get("buildingsTrans").toString();
//			}
//			
//			hasura changes end
			
		} catch (CBXBusinessException | CBXException e) {
			logger.debug("Exception {}", e.getStackTrace().toString());
		}
		return sExportData;
	}

}
