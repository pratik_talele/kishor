package com.intellect.igtb.dcp.service;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.intellect.igtb.dcp.caf.gqlactions.models.ActionEnums.gql_condition;
import com.intellect.igtb.dcp.cloudbocommons.exception.CBXBusinessException;
import com.intellect.igtb.dcp.cloudbocommons.exception.CBXException;
import com.intellect.igtb.dcp.cloudbocommons.gqlactions.models.FieldValidationResult;
import com.intellect.igtb.dcp.cloudbocommons.gqlactions.models.FieldValidationResult.val_status;
import com.intellect.igtb.dcp.export.model.ExportContext;
import com.intellect.igtb.dcp.export.model.Title;
import com.intellect.igtb.dcp.export.service.CBXSExportService;
import com.intellect.igtb.dcp.export.service.ReportService;
import com.intellect.igtb.dcp.models.IEscrowExportFields;
import com.intellect.igtb.dcp.models.IEscrowExportFields.gqlActionEnum;
import com.intellect.igtb.dcp.utilities.Utilities;

@Component
public class PrincipalForHasura extends CBXSExportService {
	private static final Logger logger = LoggerFactory.getLogger(PrincipalExport.class);


	private Utilities utils=new Utilities();
	private String actionName;
	
	HashMap<String, FieldValidationResult> valResponseMap = new HashMap<String, FieldValidationResult>();

	@Autowired
	ReportService reportService;

	@Override
	public ExportContext init(final Map<String, String> httpHeaders, final Map<String, Object> payload)
			throws CBXException {
		logger.debug("IN init - PrincipalExport {}", payload.toString());
		actionName = IEscrowExportFields.gqlActionEnum.summaryPrincipalAccountInput.name();
		logger.info("actionName : {} ",actionName);
		exportContext = super.init(httpHeaders, payload);
		exportContext.setApi_action(actionName);
		logger.debug("OUT init - PrincipalExport");
		return exportContext;
	}

	@Override
	public Map<String, FieldValidationResult> validate() {
		logger.debug("IN -> PrincipalExport.validate");
		final FieldValidationResult valResult = new FieldValidationResult(val_status.OK);
		valResponseMap = new HashMap<String, FieldValidationResult>();
		Map<String, Object> payload = exportContext.getReq_payload();
//		String format = (String) payload.get("format");

		String format = this.getFormat(payload);
		
		if (format == null || format.isEmpty()) {
			valResult.setFieldName("format");
			valResult.setErrorCode("format required");
			valResult.setStatus(val_status.FAIL);
			String fieldMessage = "format Required";
			valResult.setErrorMessage(fieldMessage);
		}

		utils.addValidationResult(valResult, valResponseMap);
		logger.debug("OUT -> PrincipalExport.validate");
		return valResponseMap;
	}

	@Override
	public String getQueryName(Map<String, Object> payload) {
//		String groupCriteria = (String) payload.get("groupCriteria");
		final String groupCriteria = (String) utils.getValueByField(payload, actionName, IEscrowExportFields.groupCriteria);
		String querName = "principal_accounts";
		return querName;
	}

	@Override
	public String getFormat(Map<String, Object> payload) {
		//String format = (String) payload.get("format");
		final String format = (String) utils.getValueByField(payload, actionName, IEscrowExportFields.FORMAT);
		return format;
	}

	@Override
	public Boolean isGrouped(Map<String, Object> payload) {
		//Boolean isGrouped = (Boolean) payload.get("isGrouped");
		final Boolean isGrouped = (Boolean) utils.getValueByField(payload, actionName, IEscrowExportFields.IS_GROUPED);
		return false;
	}

	@Override
	public Title getTitle(Map<String, Object> payload) {
		Title title = new Title();
		final String titleKey = (String) utils.getValueByField(payload, actionName, IEscrowExportFields.summary_name);
		/* title.setKey((String) payload.get("summary_name")); */
		title.setKey(titleKey);
		/* title.setKey((String) payload.get("summary_name")); */
		return title;
	}

	@Override
	public String getHeaderBlocksConfig(Map<String, Object> payload) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getGridConfigAsString(Map<String, Object> payload) { 
		String sGridConfigData = reportService.loadContent("grid_list/principalGrid.json");
		return sGridConfigData;
	}

	@Override
	public String getHeaderData(Map<String, Object> payload) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getExportData(Map<String, Object> payload) {
		String sExportData = "";
		try {
			Map<String, Object> eaCompensationDataExportMap = utils.getVASummaryForHasuraList(payload, exportContext);
			sExportData = eaCompensationDataExportMap.get("vam_summary").toString();

		} catch (CBXBusinessException | CBXException e) {
			logger.debug("Exception {}", e.getStackTrace().toString());
		}
		return sExportData;
	}


}
